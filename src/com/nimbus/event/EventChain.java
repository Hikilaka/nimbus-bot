package com.nimbus.event;

import java.util.Deque;
import java.util.LinkedList;

public final class EventChain<E extends Event> {

	private final Deque<EventListener<E>> listeners = new LinkedList<>();

	public void addToFront(EventListener<E> listener) {
		listeners.addFirst(listener);
	}

	public void addToBack(EventListener<E> listener) {
		listeners.addLast(listener);
	}

	public void remove(EventListener<E> listener) {
		listeners.remove();
	}

	public EventChainContext<E> createEventHandlerChain(E event) {
		return new EventChainContext<E>(event, listeners.iterator());
	}

}