package com.nimbus.event;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unchecked", "rawtypes"})
public final class EventDispatcher {

	private Map<Class<? extends Event>, EventChain<?>> listenerChains = new HashMap<>();

	public void fireEvent(Event event) {
		EventChain chain = listenerChains.get(event.getClass());

		if (chain != null) {
			EventChainContext ctx = chain.createEventHandlerChain(event);
			ctx.doAll();
		}
	}

	public void addListener(Class<? extends Event> eventClass, EventListener<?> listener) {
		if (!listenerChains.containsKey(eventClass)) {
			EventChain chain = new EventChain();
			chain.addToBack(listener);
			listenerChains.put(eventClass, chain);
		} else {
			EventChain chain = listenerChains.get(eventClass);
			chain.addToBack(listener);
		}
	}

	public void removeChain(Class<? extends Event> eventClass) {
		listenerChains.remove(eventClass);
	}

	public EventChain getChain(Event event) {
		return listenerChains.get(event.getClass());
	}

}