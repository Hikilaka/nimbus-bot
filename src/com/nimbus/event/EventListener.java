package com.nimbus.event;

public abstract class EventListener<E extends Event> {

	public abstract void handle(E event, EventChainContext<E> ctx);
	
}
