package com.nimbus.script;

import com.nimbus.bot.NimbusContext;

public final class ScriptManager {

	private final NimbusContext ctx;

	private Script currentScript;

	private boolean scriptRunning = false;

	public ScriptManager(NimbusContext ctx) {
		this.ctx = ctx;
	}

	public void setScript(Script script) {
		stopScript();
		currentScript = script;
	}

	public void startScript() {
		if (currentScript != null) {
			if (!scriptRunning) {
				currentScript.onStart();
				scriptRunning = true;
				ctx.write("Started script "  + currentScript.getName());
			}
		} else {
			ctx.write("Please select a script first");
		}
	}

	public void stopScript() {
		if (currentScript != null) {
			if (scriptRunning) {
				currentScript.onStop();
				scriptRunning = false;
				ctx.write("Script stopped");
			}
		} else {
			ctx.write("Please select a script first");
		}
	}

	public boolean isScriptRunning() {
		return scriptRunning;
	}

}
