package com.nimbus.script;

import com.nimbus.bot.NimbusContext;

public abstract class Script {

	protected final NimbusContext ctx;

	public Script(NimbusContext ctx) {
		this.ctx = ctx;
	}
	
	public abstract int tick();

	public abstract String getName();
	
	public abstract void onStart();
	
	public abstract void onStop();

}