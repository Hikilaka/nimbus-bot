package com.nimbus.util;

public final class Util {

	public static String formatTime(long time) {
		StringBuilder t = new StringBuilder();
		long total_secs = time / 1000;
		long total_mins = total_secs / 60;
		long total_hrs = total_mins / 60;
		
		int secs = (int) total_secs % 60;
		int mins = (int) total_mins % 60;
		int hrs = (int) total_hrs % 24;
		
		if (hrs < 10) {
			t.append("0");
		}
		t.append(hrs);
		t.append(":");
		if (mins < 10) {
			t.append("0");
		}
		t.append(mins);
		t.append(":");
		if (secs < 10) {
			t.append("0");
		}
		t.append(secs);
		return t.toString();
	}

}
