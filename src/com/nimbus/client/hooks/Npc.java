package com.nimbus.client.hooks;

public interface Npc extends MobileEntity {

	public NpcDef getNPCDef();

	public boolean isDefinitionSet();

}
