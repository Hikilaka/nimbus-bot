package com.nimbus.client.hooks;


public interface Client {

	public void processedClientUpdates();

	public Player getMyPlayer();

	public Player[] getPlayers();

	public Npc[] getNPCs();

	public Deque[][][] getGroundArray();

	public String[] getChatMessages();

	public String[] getChatNames();

	public int[] getChatTypes();

	public int[] getSessionNpcList();

	public int getSessionNpcCount();

	//public Stream getStream(); TODO hook it correctly
}
