package com.nimbus.client.hooks;

public interface NpcDef {
	
    public String getName();
    
    public boolean isClickable();
    
}
