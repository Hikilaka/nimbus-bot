package com.nimbus.bot.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.nimbus.bot.NimbusContext;
import com.nimbus.bot.ui.ScriptSelector;

public final class NimbusKeyListener implements KeyListener {

	private final NimbusContext ctx;

	public NimbusKeyListener(NimbusContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Do nothing..
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_F1:
			new ScriptSelector(ctx);
			break;
		case KeyEvent.VK_F2:
			ctx.getScriptManager().startScript();
			break;
		case KeyEvent.VK_F3:
			ctx.getScriptManager().stopScript();
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// Do nothing..
	}

}
