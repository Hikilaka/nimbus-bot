package com.nimbus.bot.input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;

import com.nimbus.bot.NimbusContext;
import com.nimbus.bot.ui.AccountManager;
import com.nimbus.bot.ui.ScriptSelector;

public final class NimbusActionListener implements ActionListener, ItemListener {

	private final NimbusContext ctx;

	public NimbusActionListener(NimbusContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void actionPerformed(ActionEvent e) {	
		switch (e.getActionCommand()) {
		case "Select script":
			new ScriptSelector(ctx);
			break;
		case "Start script":
			ctx.getScriptManager().startScript();
			break;
		case "Stop script":
			ctx.getScriptManager().stopScript();
			break;
		case "Exit":
			int option = JOptionPane.showOptionDialog(ctx.getFrame(), "Are you sure you want to exit?", "Nimbus", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, new String[] { "Yes", "No" }, 1);
			if (option == 0) {
				System.exit(0);
			}
			break;
		case "Account manager":
			new AccountManager(ctx);
			break;
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getItem() instanceof String) {
			String item = String.class.cast(e.getItem());
			switch (item) {
			case "Enable autologin":
				//TODO: actually enable auto login..
				ctx.write("Autologin " + (ctx.toggleAutoLogin() ? "enabled" : "disabled"));
				break;
			case "Disable graphics":
				//TODO: actually disable graphics..
				// ctx.getClient().toggleGraphics(); possibly?
				ctx.write("Graphics " + (ctx.toggleGraphics() ? "disabled" : "enabled"));
				break;
			}
		}
	}

}
