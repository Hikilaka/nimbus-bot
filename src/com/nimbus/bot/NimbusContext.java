package com.nimbus.bot;

import java.applet.Applet;

import javax.swing.JFrame;

import com.nimbus.client.hooks.Client;
import com.nimbus.event.EventDispatcher;
import com.nimbus.script.ScriptManager;
import com.nimbus.util.Util;

public final class NimbusContext {

	private final Nimbus nimbus;

	private final EventDispatcher eventDispatcher = new EventDispatcher();

	private final ScriptManager scriptManager = new ScriptManager(this);

	private boolean autoLogin, graphics;

	public NimbusContext(Nimbus nimbus) {
		this.nimbus = nimbus;
	}

	public void write(String string) {
		nimbus.textArea.append("[" + Util.formatTime(System.currentTimeMillis()) + "] " + string + '\n');
	}

	public JFrame getFrame() {
		return nimbus.frame;
	}

	public Applet getApplet() {
		return nimbus.applet;
	}

	public Client getClient() {
		return Client.class.cast(nimbus.applet);
	}

	public EventDispatcher getEventDispatcher() {
		return eventDispatcher;
	}

	public ScriptManager getScriptManager() {
		return scriptManager;
	}

	public boolean isAutoLogin() {
		return autoLogin;
	}

	public boolean toggleAutoLogin() {
		autoLogin = !autoLogin;
		return autoLogin;
	}

	public boolean isGraphics() {
		return graphics;
	}

	public boolean toggleGraphics() {
		graphics = !graphics;
		return graphics;
	}

}
