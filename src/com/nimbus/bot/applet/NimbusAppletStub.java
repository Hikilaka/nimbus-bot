package com.nimbus.bot.applet;

import java.applet.AppletContext;
import java.applet.AppletStub;
import java.net.URL;

import com.nimbus.bot.NimbusContext;
import com.nimbus.util.Config;

public final class NimbusAppletStub implements AppletStub {

	private final NimbusAppletContext appletCtx;

	private URL url;

	public NimbusAppletStub(NimbusContext nimbus) {
		this.appletCtx = new NimbusAppletContext(nimbus);

		try {
			url = new URL(Config.APPLET_STUB_URL);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public URL getDocumentBase() {
		return url;
	}

	@Override
	public URL getCodeBase() {
		return url;
	}

	@Override
	public String getParameter(String name) {
		return null;
	}

	@Override
	public AppletContext getAppletContext() {
		return appletCtx;
	}

	@Override
	public void appletResize(int width, int height) {

	}

}
