package com.nimbus.bot.applet;

import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AudioClip;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;

import com.nimbus.bot.NimbusContext;

public final class NimbusAppletContext implements AppletContext {

	private final NimbusContext ctx;

	public NimbusAppletContext(NimbusContext nimbus) {
		this.ctx = nimbus;
	}

	@Override
	public AudioClip getAudioClip(URL url) {
		ctx.write("NimbusAppletContext.getAudioClip(" + url + ")");
		return null;
	}

	@Override
	public Image getImage(URL url) {
		ctx.write("NimbusAppletContext.getImage(" + url + ")");
		return null;
	}

	@Override
	public Applet getApplet(String name) {
		ctx.write("NimbusAppletContext.getApplet(" + name + ")");
		return null;
	}

	@Override
	public Enumeration<Applet> getApplets() {
		ctx.write("NimbusAppletContext.getApplets()");
		return null;
	}

	@Override
	public void showDocument(URL url) {
		ctx.write("NimbusAppletContext.showDocument(" + url + ")");
	}

	@Override
	public void showDocument(URL url, String target) {
		ctx.write("NimbusAppletContext.showDocument(" + url + ", " + target + ")");
	}

	@Override
	public void showStatus(String status) {
		ctx.write("NimbusAppletContext.showStatus(" + status + ")");
	}

	@Override
	public void setStream(String key, InputStream stream) throws IOException {
		ctx.write("NimbusAppletContext.setStream(" + key + ", " + stream + ")");
	}

	@Override
	public InputStream getStream(String key) {
		ctx.write("NimbusAppletContext.getStreamKeys(" + key + ")");
		return null;
	}

	@Override
	public Iterator<String> getStreamKeys() {
		ctx.write("NimbusAppletContext.getStreamKeys()");
		return null;
	}

}
