package com.nimbus.bot;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.CheckboxMenuItem;
import java.awt.Dimension;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.TextArea;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.nimbus.bot.applet.NimbusAppletStub;
import com.nimbus.bot.input.NimbusActionListener;
import com.nimbus.bot.input.NimbusKeyListener;

public final class Nimbus {

	public static void main(String[] args) throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Nimbus();
			}
		});
	}

	private final NimbusContext ctx = new NimbusContext(this);

	protected final JFrame frame = new JFrame("Nimbus - by Hikilaka & Silabsoft");

	protected final TextArea textArea = new TextArea(null, 6, 70, TextArea.SCROLLBARS_VERTICAL_ONLY);

	protected Applet applet;

	private Nimbus() {
		ctx.write("Starting up Nimbus..");
		
		ClassLoader loader = Nimbus.class.getClassLoader();
		try {
			Class<?> clientClass = loader.loadClass("Client");
			applet = Applet.class.cast(clientClass.newInstance());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		applet.setStub(new NimbusAppletStub(ctx));
		applet.addKeyListener(new NimbusKeyListener(ctx));
		applet.setPreferredSize(new Dimension(765, 503));

		MenuBar bar = new MenuBar();
		NimbusActionListener actionListener = new NimbusActionListener(ctx);

		Menu file = new Menu("File");
		MenuItem sel = new MenuItem("Select script");
		sel.addActionListener(actionListener);
		file.add(sel);
		MenuItem sta = new MenuItem("Start script");
		sta.addActionListener(actionListener);
		file.add(sta);
		MenuItem sto = new MenuItem("Stop script");
		sto.addActionListener(actionListener);
		file.add(sto);
		file.addSeparator();
		MenuItem exit = new MenuItem("Exit");
		exit.addActionListener(actionListener);
		file.add(exit);
		bar.add(file);

		Menu settings = new Menu("Settings");
		MenuItem manager = new MenuItem("Account manager");
		manager.addActionListener(actionListener);
		settings.add(manager);
		CheckboxMenuItem al = new CheckboxMenuItem("Enable autologin");
		al.addItemListener(actionListener);
		settings.add(al);
		CheckboxMenuItem gfx = new CheckboxMenuItem("Disable graphics");
		gfx.addItemListener(actionListener);
		settings.add(gfx);
		bar.add(settings);

		textArea.setEditable(false);
		textArea.setPreferredSize(new Dimension(765, 100));

		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMenuBar(bar);
		frame.add(applet);
		frame.add(textArea, BorderLayout.SOUTH);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.pack();
		frame.setLocationRelativeTo(null);

		applet.init();
		applet.start();
	}

}